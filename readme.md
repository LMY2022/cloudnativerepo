cloudnativerepo - readme
====

## Vagrant

### Übersicht


## Docker

#### Übersicht

Docker ist eine Software zur Containevirtualisierung von Anwendungen. Mithilfe der Containevirtualisierung können Anwendungen unabhängig vom unterliegenden Betriebsystem verwendet werden. Die Container sind auch abgeschottet von anderen Container, die auf dem Betriebsystem laufen.

#### Wissensstand

Ich habe grundlegende Kenntnisse von Linux und bereits erste Erfahrungen zum Thema Vagrant gesammelt. Ich habe bereits zuvor von Docker gehört, allerdings noch nie damit zu tun gehabt.


#### Umgebung erstellen

Zuerst wird natülich Docker auf dem lokalen Rechner installiert. Nach einem Neustart des Computers kann es dann auch schon losgehen.
Ich erstelle ein Image aus dem Dockerfile, welches ich lokal auf meinem Computer habe:

```
Get-Content Dockerfile | docker build -
```

Danach benenne ich zur Klarheit das Image um:

```
docker tag 83a2e2c76573 apache2image
```

Jetzt wird ein Container mit dem Image erstellt und gestartet:

```
docker run --rm -d -p 8080:80 -v /web:/var/www/html --name Apache2Container 83a2e2c76573
```

Jetzt ist der Container mit Apache2 darin gestartet. Wenn dies erfolgreich funktioniert hat, kann man
via dem folgenden Befehl auf die Kommandozeile zugreiffen:

```
docker exec -it Apache2Container /bin/bash
```

Ich gehe nun mit "exit" zurück zur Powershell Konsole und zeige apache den Pfad zum HTML-File

```
docker cp C:\Users\Lars\OneDrive\OneDriveDesktop\M300\M300-LB02\index.html Apache2Container:/var/www/html/
```

Unter "localhost:8080" kann die Webseite jetzt aufgerufen werden.



#### Befehle
                            
							
| Befehl                       | Effekt                         |
| ---------------------------- |:------------------------------:|
| docker stats | Zeigt ressourcennutzung der Container in Echtzeit |
| docker info | Zeigt Informationen zum System |
| docker login | Im Docker Hub anmelden |

                                        
| Befehl                       | Effekt                         |
| ---------------------------- |:------------------------------:|
| docker pull *imagename* | Lädt das Image vom Docker Hub herunter  |
| docker run --name *imageID* | Startet ein bestimmtes Image |
| docker run -it *imageID* | Startet ein bestimmtes Image und zeigt die Befehlszeile an |
| docker build -t *imagename* | Erstellt ein Image vom Dockerfile mit dem festgelegten Namen im Verzeichnis |
| docker images | Zeigt eine Übersicht der Images an |
| docker image rm *imagename* | Löscht das Image |
| docker image rmi *imagename* | Löscht das Image |
| docker rmi `docker images -q -f dangling=true` | Zwischenimages löschen |

                                                
| Befehl                       | Effekt                         |
| ---------------------------- |:------------------------------:|
| docker start *ContainerID* | Startet den Container |
| docker container ls | Zeigt alle laufenden Container an. |
| docker container start -i *containername* | Startet einen gestoppten Container |
| docker container stop *containername* | Stoppt den laufenden Container. |
| docker kill | Stop den Hauptprozess des Containers |
| docker network ls | Zeit alle Netzwerke an. |
| docker rm -f $(docker ps -aq) | Löscht alle laufenden und gestoppten Container |
| docker logs --tail 100 *containername* | Zeigt die 100 letzten Zeilen des Container logs an. |
| docker ps  | Zeigt alle aktiven Container an |
| docker ps -a | Zeigt alle Container an |
| docker run --rm -d -p 3306:3306 mysql | Mysql an port 3306 binden für Internetzugriff |
| docker inspect *containername* | Zeigt Infos zum Container an |


#### Mögliche Angriffe

- Kernel Exploit
- Denial-of-Service (DoS)
- Container Breakouts
- Vergiftete/manipulierte Images


#### Massnahmen zur Containersicherheit

- Least Privilege
- Ressourcen begrenzen
- Container nach Host trennen
- Container absichern


## Kubernetes

#### Einführung

Kubernetes ist ein Open-Source system zur Bereitstellung, Skalierung und Verwaltung von Container-Anwendungen. Docker ist dabei nur eines der Container-Tools, die von Kubernetes unterstützt werden.
Da es Umgebungen geben kann, in denen zwei Container miteinander Kommunizieren müssen, werden Container in Kubernetes in Pods zusammengefasst, selbst wenn es sich nur um einen Container handelt.
Innerhalb eines Pods können die Container mittels eines 
Pods können untereinander kommunizieren und werden von sogenannten "Worker Nodes" verwaltet.
Mittels eines "Service" kann dann von aussen auf die Umgebung zugegriffen werden und er kümmert sich um das Load Balancing.
Mittels "Deployments" können ganze Umgebungen schnell und einfach aufgesetzt werden. Zudem stellt bei Deployments der Master sicher,dass das Deployments im "Desired state" bleibt. Wenn Pods direkt deployed werden, werden sie vom Master nicht wieder repariert.


#### Befehle

| Befehl                       | Effekt                         |
| ---------------------------- |:------------------------------:|
| kubectl create -f *datei* | Erstellt eine Ressource von einer Datei |
| Kubectl expose | ? |
| kubectl run *imagename*  | Startet das angegebene Image in einem Container und legt ihn in ein Pod |
| kubectl set | Updaten und Bearbeiten von Images, Environment variables, resources, usw. |
| kubectl explain *api-resource*  | Zeigt Details zu einer Ressource an. |
| kubectl delete *ressource name* | Löscht die angegebene Ressource. |
| kubctl get all | Zeigt alle aktuellen Ressourcen an |


## EC2 Webserver mit UserData aufsetzen

1. Bucket erstellen:
	* Namen vergeben
	* "Create Bucket"
2. Dateien in Bucket Hochladen:
	* Bilder & HTML
3. IAM Rolle erstellen:
	* Mit Use Case "EC2"
	* Add permissions: "AmazonS3ReadOnlyAccess"
	* Role Name vergeben: "S3-ReadOnly"
4. Rolle an EC2 Instanz vergeben:
	* Instanz anwählen > Actions > Security > Modigy IAM Role > "S3-ReadOnly"
	* Bucket kann jetzt von der Instanz gelesen werden
5. Neue EC2 Instanz erstellen:
	* Name: LMY2022-Webserver
	* Key Pair auswählen
	* Select existing security Group: "WebAccess-LMY"
	* IAM Instance Profile: S3-Readonly
	* User Data: Script unten hineinkopieren
6. Port 80 Freigeben in Security Group:
	* Security Group "WebAccess-LMY" öffnen
	* Inbound Rules: Type: HTTP, Source Anywhere-IPv4
	* Speichern
7. Seite anhand Public-IP der EC2 Instanz öffnen im Browser

### User Data Script:
```
#!/bin/bash
yum update -y
yum install -y httpd
systemctl start httpd
systemctl enable httpd
aws s3 cp s3://lmy22-bucket/ami-webserver.txt /var/www/html/index.html
```

## MQTT Mosquito mit EC2 VM

* EC2 VM mit Ubuntu 20.04 aufsetzen.

### Provisionierung:
```
sudo apt-get update
sudo apt-get install mosquitto -y
sudo apt-get install mosquitto-clients -y
sudo service mosquitto stop
mosquitto -v
```
* Public-IP der VM notieren und in Script einfügen.
* Port 1883 freigeben via Security Group.

### Python Script für M5GO:
```
from m5stack import *
from m5ui import *
from uiflow import *
from m5mqtt import M5mqtt
import time

setScreenColor(0x222222)

label0 = M5TextBox(99, 96, "label0", lcd.FONT_Default, 0xFFFFFF, rotate=0)

m5mqtt = M5mqtt('M5GO', '54.234.199.67', 1883, '', '', 300)
m5mqtt.start()
while True:
  wait(1)
  m5mqtt.publish(str('M5Output'), str('msg'), )
  wait_ms(2)
```
Auf den M5GO laden und ausführen, anschliessend wird von der Konsole auf der EC2 VM folgendes empfangen:
```
1668595715: Received PUBLISH from M5GO (d0, q0, r0, m0, 'M5Output', ... (3 bytes))
```


## Container und Microservices

### Begriffe

| Begriff                      | Erklärung                         |
| ---------------------------- |:------------------------------:|
|Dockerfile | Infrastructure As Code-Datei in klartext welche aus dem ein Image Schritt für Schritt erstellt wird.|
|Image |	Wird aus dem Dockerfile erstellt und aus dem eine laufende Instanz erstellt werden kann.|
|Container |	Das aus einem Image resultierende Paket aus Software. Üblicherweise hat der Container alle Software enthalten, die der Service zum laufen braucht.|
|Tag |	Ein Tag wird dazu verwendet, die Images besser unterscheiden zu können. Oft wird die Version des Images verwendet, es kann aber ein beliebiger Text verwendet werden. Der Tag wird z.B. gesetzt durch "docker build -t username/image_name:tag_name ."  Wenn kein Tag spezifiziert wird, wird "latest" als Tag verwendet. Dies kann zu Verwirrung führen, da das Image obwohl es den Tag "latest" hat, nicht zwingend das neuste sein muss.  Ein Image kann mehrere Tags haben.|
|Imperativ |	Befehle nacheinander manuell eingeben.|
|Deklarativ |	Befehle wurden in einem Infrastructure As Code-File aufgelistet und werden automatisch sequenziell abgearbeitet. Üblicherweise ein YAML oder JSON File.|
|Worker Node |	|
|app.js | 	Die eigentliche JavaScript applikation|
|package.json | Enthält Meta Daten und Dependencies|
|docker-compose.yaml |	Config File für Docker um mehrere Container in einem Worker Node aufzubauen.|
	
| Befehl                      | Effekt                         |
| ---------------------------- |:------------------------------:|
|docker --version |	Shows the currently installed Docker-Version|
|docker pull <image name> |	Pull an Image from hub.docker.com|
|docker build <Dockerfile path> | Build an image from a specified Dockerfile.|
|docker run --name <desired container name> -it -d -p <port nr container>:<port nr host> <image name> | Create a Container from an Image  -it = ?  -d = runs the container detatched, meaning in the background. Therefore the console window will not be occupied.  Details can be shown using "docker ps".  <image name> can stem from a repository like "registry.gitlab.com/ser-cal/docker-bootstrap/webserver_one:1.0"|
|docker stop <container-id> |	Stops the specified container.|
|docker kill <container-id> |	Stops the container immediately, without grace period.|
|docker ps -a |	list the running containers  -a = show all (running and exited)|
|docker exec -it <container-id> bash | Access Container via Bash|
|docker commit <container-id> <username>/<image name> | Create a new image of an edited container on the local system.|
|docker login <username>/<image name> | Login to DockerHub repository.|
|docker push <username>/<image name> | Push an image to docker hub repository.|
|docker images | List locally stored docker images|
|docker rm <container-id> | Delete a stopped container|
|docker rmi <image-id> | Delete an Image from local storage. Only works if Image is unused.|
|docker rmi <container-id> | Remove an Image from a (stopped?) Container.|

• Platzen der Dotcom-Bubble führte zu strengeren Finanzen in der IT.
• Virtualisierung durch Virtuelle Maschinen führe zu besserer Auslastung der Hardware.
• Containerization ist eine weiterentwicklung dieser Virtualisierung und führt zu besserer Auslastung

Vorteile:
• Tiefere Lizenzkosten
• Wenoger Administrationskosten
• Wenoger Produktionskosten
• Hohe flexibilität
• Redundanz & Unabhängigkeit vom Rest der Umgebung

## 1. Docker Image und Container (Part 1)

Lokal auf Laptop (von Gitlab-Repo ein Zip-file erstellt und downgeloaded)
Aus demselben Verzeichnis werden auf GitBash folgende Kommandos ausgeführt:

scp docker-bootstrap-main.zip ubuntu@10.3.44.26:/home/ubuntu	#(Achtung! Eigene IP-Adresse)
ssh ubuntu@10.3.44.26		#(Achtung! Eigene IP-Adresse)
unzip docker-bootstrap-main.zip
cd docker-bootstrap-main
cd first-container
ls -al

cat Dockerfile

#Image aus GitLab herunterladen, üblicherweise wird DockerHub verwendet. Anschliessend werden die Schritte aus dem Dockerfile durchgeführt. Das Image wird aber hier erst erstellt und noch nicht gestartet.  
docker image build -t registry.gitlab.com/ser-cal/docker-bootstrap/webserver_one:1.0 .  

#Zum überprüfen wird mit grep alles ausgegeben was "webserver" im Namen hat. Die nummer nach der Version ist die Image-ID  
docker image ls | grep -i webserver

docker login registry.gitlab.com

docker build -t registry.gitlab.com/lmy2022/cloudnativerepo/webserver_one:1.0 .

#Damit das Image später von einer anderen Umgebung wieder verwendet werden kann, wird es abgelegt. Beispielsweise auf DockerHub oder wie hier auf GitLab.  
docker push registry.gitlab.com/lmy2022/cloudnativerepo/webserver_one:1.0 

#Docker Image wird gestartet  
docker container run -d --name container-webserver -p 8080:8080 registry.gitlab.com/ser-cal/docker-bootstrap/webserver_one:1.0

#-> Service ist nun erreichbar unter  10.3.44.26:8080  

#Nun wird der Container gelöscht und das Image von meinem eigenen Repository geholt um ihn neu aufzubauen.  
docker container stop container-webserver

#Zeige alle Container an, auch die gestoppten  
docker container ls -a | grep -i container-webserver

#Trotzdem kann das darunterliegende Image nicht gelöscht werden, wenn der Container nur gestoppt ist  
docker image list | grep -i webserver_one
docker image rm <Image-ID>

#Container löschen  
docker container rm <Container-ID>

#Dann erst das Image löschen  
docker rmi a80709dbfd56

#-> Falls mehrere Container das Image verwenden, mit  "docker image list | grep -i <Image-ID>" alle Container anzeigen lassen, die das Image verwenden und dann mit "docker rmi <repo-name>" vom image lösen ("Untaggen")  

#Container aus dem eigenen Repo starten  
docker container run -d --name container-webserver -p 8080:8080 registry.gitlab.com/lmy2022/cloudnativerepo/webserver_one:1.0


### Docker-Compose (Part 2)

#In Verzeichnis /home/ubuntu/docker-bootstrap-main/multi-container wechseln.  
cd /home/ubuntu/docker-bootstrap-main/multi-container

#Prüfen ob das Docker-Compose Package installiert ist  
docker-compose version

#Falls nicht vorhanden, installieren  
sudo apt install docker-compose -y

#docker-compose starten  
docker-compose up -d

Im browser unter http://10.3.44.26:5000/ prüfen, ob es funktioniert.

#docker-compose beenden  
docker-compose down

#prüfen der Container beendet wurde.  
docker container ls -a
